'use strict';

angular.module('globalBookingSystemApp')
	.component('noticeBoard', {
		templateUrl: 'components/noticeBoard/noticeBoard.html',
		controller: function ($translatePartialLoader) {
			this.test = 'noticeBoard component';
			console.log(this.test);
			$translatePartialLoader.addPart('noticeBoard');
		}
	});
