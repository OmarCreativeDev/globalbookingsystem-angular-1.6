'use strict';

angular.module('globalBookingSystemApp')
	.component('footer', {
		templateUrl: 'components/footer/footer.html',
		controller: function ($translatePartialLoader) {
			this.component = 'footer component';
			console.log(this.component);

			$translatePartialLoader.addPart('footer');
		}
	});
