'use strict';

angular.module('globalBookingSystemApp')
	.component('homeTile', {
		templateUrl: 'components/homeTile/homeTile.html',
		bindings: {
			name: '<',
			state: '<',
			image: '<',
		},
		controller: function ($translatePartialLoader) {
			this.test = 'home-tile component';
			$translatePartialLoader.addPart('homeTile');
		}
	});
