'use strict';

angular.module('globalBookingSystemApp')
	.component('datePicker', {
		templateUrl: 'components/datePicker/datePicker.html',
		controller: function () {
			console.log('datePicker component');
			this.date = new Date();
			this.dateOptions = {
				formatYear: 'yy',
				maxDate: new Date(2020, 5, 22),
				minDate: new Date(),
				startingDay: 1,
				showWeeks: false
			};

			this.open = function () {
				this.isOpen = true;
			}
		}
	});
