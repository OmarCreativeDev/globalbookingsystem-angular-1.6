'use strict';

angular.module('globalBookingSystemApp')
	.component('header', {
		templateUrl: 'components/header/header.html',
		controller: function ($translatePartialLoader) {
			this.test = 'header component';
			console.log(this.test);

			$translatePartialLoader.addPart('header');
		}
	});
