'use strict';

angular.module('globalBookingSystemApp')
	.component('login', {
		templateUrl: '/components/login/login.html',
		controller: function ($state, $translatePartialLoader) {
			this.goTo = function() {
				$state.go('home');
			}
			$translatePartialLoader.addPart('login');
		}
	});
