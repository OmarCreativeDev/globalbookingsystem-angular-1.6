'use strict';

/**
 * @ngdoc overview
 * @name globalBookingSystemApp
 * @description
 * # globalBookingSystemApp
 *
 * Main module of the application.
 */
angular
	.module('globalBookingSystemApp', [
		'ngCookies',
		'ngSanitize',
		'pascalprecht.translate',
		'ui.bootstrap',
		'ui.router'
	])
	.config(function($cookiesProvider, $stateProvider, $urlRouterProvider, $translateProvider) {

		$cookiesProvider.defaults.path = '/';

		$urlRouterProvider.otherwise('/login');

		$stateProvider
			.state('login', {
				url: '/login',
				templateUrl: 'views/login/login.html',
				data: { pageTitle: 'Login' }
			})
			.state('home', {
				url: '/home',
				templateUrl: 'views/home/home.html',
				data: { pageTitle: 'Home' }
			})
			.state('dropAtAirport', {
				url: '/drop-at-airport',
				templateUrl: 'views/dropAtAirport/dropAtAirport.html',
				data: { pageTitle: 'Drop at airport' }
			})
			.state('styleGuide', {
				url: '/styleGuide',
				templateUrl: 'views/styleGuide/styleGuide.html',
				data: { pageTitle: 'Style Guide' }
			});

		$translateProvider.useLoader('$translatePartialLoader', {
			urlTemplate: '/components/{part}/{part}.i18n.{lang}.json'
		});

		$translateProvider.preferredLanguage('en');

		// Enable escaping of HTML
		$translateProvider.useSanitizeValueStrategy('sanitize');
	})
	.run(function($rootScope, $state, $translate) {
		$rootScope.$state = $state;

		$rootScope.$on('$translatePartialLoaderStructureChanged', function () {
			$translate.refresh();
		});
	});
